case "$1" in
  "swap") free -m|awk '$1=="Mem:"{print $2*2}';;
  "devl") lsblk -dp|awk '$6=="disk"{print $1}';;
esac
