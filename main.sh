$archinstallsh_dev=""


clear
echo -e "arch簡単インストール\n\n　インストールするデバイスを教えてください\n"
bash f.sh devl
echo "上記がインストールできるデバイスリストです"
read -p "インストールしたいデバイス名を入力してください: " archinstallsh_dev

while :
do
  clear
  echo -e "arch簡単インストール\n\n　"$archinstallsh_dev" が選択されました\n"
  fdisk -l $archinstallsh_dev
  read -p "上記のデバイスのデータがすべて消えます。y/n: " flag
  if [ "$flag" = "y" ]; then
    break
  elif [ "$flag" = "n" ]; then
    clear
    echo -e "arch簡単インストール\n\n　インストールするデバイスを教えてください\n"
    bash f.sh devl
    echo "上記がインストールできるデバイスリストです"
    read -p "インストールしたいデバイス名を入力してください: " archinstallsh_dev
  fi
done
clear
echo $flag
